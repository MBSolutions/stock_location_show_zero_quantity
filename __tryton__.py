#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Stock Location Show Products with Zero Quantity',
    'name_de_DE': 'Zeige im Lager Produkte mit der Menge Null',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    This module provides showing products in a location with a
    quantity of zero.
    ''',
    'description_de_DE': '''
    Dieses Modul ermöglicht es, Produkte mit der Menge null
    im Lager anzuzeigen.
    ''',
    'depends': [
        'stock',
    ],
    'xml': [
        'stock.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
